<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Destination extends Model
{
    protected $fillable = [
        'guid', 'name', 'description', 'address', 'rif', 'phones', 'id_estado', 'id_municipio', 'id_parroquia', 'id_ciudad',
    ];

    public function order()
    {
        return $this->hasMany(Order::class);
    }
}
