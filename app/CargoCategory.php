<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CargoCategory extends Model
{
    protected $fillable = [
        'guid', 'name', 'description',
    ];

    public function order()
    {
        return $this->hasMany(Order::class);
    }
}
