<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'guid', 'name', 'email', 'rif', 'phones', 'address',
    ];

    public function order()
    {
        return $this->hasMany(Order::class);
    }
}
