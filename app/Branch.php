<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = [
        'name', 'rif', 'email', 'manager_id', 'phones', 'address',
    ];
}
