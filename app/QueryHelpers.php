<?php

namespace App;

class QueryHelpers
{
    public static function filterQuery($filters, $query)
    {
        foreach ($filters as &$filter) {
            $field = $filter["column"];
            $operator = $filter["operator"];
            $prefix = $operator == "like" ? "%": "";
            $postfix = $operator == "like" ? "%": "";
            $value = $prefix.$filter["value"].$postfix;
            $boolean = $filter["boolean"];

            if ($boolean=="and") {
                $query->where($field, $operator, $value);
            } else {
                $query->orWhere($field, $operator, $value);
            }
        }
        return $query;
    }
}