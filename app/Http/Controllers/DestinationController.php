<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Destination;

class DestinationController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $selectArray = array('destinations.id','destinations.guid','destinations.name','destinations.description','destinations.address',
            'destinations.rif','destinations.phones','estados.estado','municipios.municipio',
            'ciudades.ciudad', 'parroquias.parroquia','estados.id_estado','municipios.id_municipio',
            'ciudades.id_ciudad', 'parroquias.id_parroquia');

        $result = Destination::join('estados', 'destinations.id_estado', '=', 'estados.id_estado')
            ->join('municipios', 'destinations.id_municipio', '=', 'municipios.id_municipio')
            ->join('ciudades', 'destinations.id_ciudad', '=', 'ciudades.id_ciudad')
            ->join('parroquias', 'destinations.id_parroquia', '=', 'parroquias.id_parroquia')
            ->paginate(12, $selectArray);

        return $this->respondSuccessGet('Ok', $result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->respondSuccessGet('Ok', Destination::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $selectArray = array('destinations.id','destinations.guid','destinations.name','destinations.description','destinations.address',
            'destinations.rif','destinations.phones','estados.estado','municipios.municipio',
            'ciudades.ciudad', 'parroquias.parroquia','estados.id_estado','municipios.id_municipio',
            'ciudades.id_ciudad', 'parroquias.id_parroquia');

        $result = Destination::where('destinations.id',$id)
            ->join('estados', 'destinations.id_estado', '=', 'estados.id_estado')
            ->join('municipios', 'destinations.id_municipio', '=', 'municipios.id_municipio')
            ->join('ciudades', 'destinations.id_ciudad', '=', 'ciudades.id_ciudad')
            ->join('parroquias', 'destinations.id_parroquia', '=', 'parroquias.id_parroquia')
            ->select($selectArray)->first();

        return $this->respondSuccessGet('Ok', $result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Destination::find($id);

        if($model->update($request->all()))
        {
            return $this->respondSuccessGet('Ok', $model);
        }

        return $this->respondFailed('Ok', $model);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Destination::find($id);

        if($model === NULL)
        {
            return $this->respondFailed('Ok', false);
        }

        return $this->respondSuccessGet('Ok', $model->delete());
    }

    public function getByGuid($guid)
    {
        $model = Destination::where('guid', $guid)->first();

        if($model === NULL)
        {
            return $this->respondFailed('Ok', false);
        }

        return $this->respondSuccessGet('Ok', $model);
    }
}
