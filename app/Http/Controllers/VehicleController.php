<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vehicle;
use Illuminate\Support\Str;

class VehicleController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Vehicle::join('vehicle_types', 'vehicles.vehicle_type_id', '=', 'vehicle_types.id')
            ->select('vehicles.*','vehicle_types.name as vehicle_type_name')
            ->paginate(12);

        return $this->respondSuccess('Ok', $result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newVehicle = $request->all();
        $newVehicle['guid'] = Str::uuid()->toString();
        $model = Vehicle::create($newVehicle);

        if($model)
        {
            return $this->show($model->id);
        }

        return $this->respondFailed('Ok', $model);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = Vehicle::where("vehicles.id", $id)
            ->join('vehicle_types', 'vehicles.vehicle_type_id', '=', 'vehicle_types.id')
            ->select('vehicles.*','vehicle_types.name as vehicle_type_name')
            ->first();

        return $this->respondSuccessGet('Ok', $result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Vehicle::find($id);

        if($model->update($request->all()))
        {
            return $this->show($model->id);
        }

        return $this->respondFailed('Ok', $model);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Vehicle::find($id);

        if($model === NULL)
        {
            return $this->respondFailed('Ok', false);
        }

        return $this->respondSuccessGet('Ok', $model->delete());
    }

    public function getByGuid($guid)
    {
        $model = Vehicle::where('guid', $guid)->first();

        if($model === NULL)
        {
            return $this->respondFailed('Ok', false);
        }

        return $this->respondSuccessGet('Ok', $model);
    }
}
