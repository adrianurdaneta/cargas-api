<?php

namespace App\Http\Controllers\Auth;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;
use App\User;
use DateTime;
use DatePeriod;
use DateInterval;

class AuthController extends BaseController
{
    /**
     * @var \Tymon\JWTAuth\JWTAuth
     */
    protected $jwt;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|max:20',
            'password' => 'required',
        ]);

        try {
            if (!$token = $this->jwt->attempt($request->only('username', 'password'))) {
                $data = null;
                $success = false;
                $message = "Usuario o contraseña errado.";

                return response()->json(compact('message','data','success'));
            }
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], 500);
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], 500);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent' => $e->getMessage()], 500);
        }

        $user = User::where("username","=",$request->only("username"))->first();
        $date = new DateTime();
        $tokenExpirationDate= $date->add(new DateInterval('P1Y'))->format('Y-m-d H:i:s');

        $data = compact('token','user','tokenExpirationDate');
        $success = true;
        $message = "Inicio de sesión exitoso";

        return response()->json(compact('message','data','success'));
    }
}