<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CargoCategory;
use Illuminate\Support\Str;

class CargoCategoryController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->respondSuccessGet('Ok', CargoCategory::paginate(12));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newCargoCategory = $request->all();
        $newCargoCategory['guid'] = Str::uuid()->toString();
        $model = CargoCategory::create($newCargoCategory);

        if($model)
        {
            return $this->show($model->id);
        }

        return $this->respondFailed('Ok', $model);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->respondSuccessGet('Ok', CargoCategory::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = CargoCategory::find($id);

        if($model->update($request->all()))
        {
            return $this->respondSuccess('Ok', $model);
        }

        return $this->respondFailed('Ok', $model);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = CargoCategory::find($id);

        if($model === NULL)
        {
            return $this->respondFailed('Ok', false);
        }

        return $this->respondSuccessGet('Ok', $model->delete());
    }

    public function getByGuid($guid)
    {
        $model = CargoCategory::where('guid', $guid)->first();

        if($model === NULL)
        {
            return $this->respondFailed('Ok', false);
        }

        return $this->respondSuccessGet('Ok', $model);
    }

    public function fullList()
    {
        return $this->respondSuccessGet('Ok', CargoCategory::all());
    }
}
