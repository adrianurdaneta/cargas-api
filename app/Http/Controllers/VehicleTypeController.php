<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VehicleType;

class VehicleTypeController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->respondSuccessGet('Ok', VehicleType::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->respondSuccess('Ok', VehicleType::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->respondSuccessGet('Ok', VehicleType::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = VehicleType::find($id);

        if($model->update($request->all()))
        {
            return $this->respondSuccess('Ok', $model);
        }

        return $this->respondFailed('Ok', $model);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = VehicleType::find($id);

        if($model === NULL)
        {
            return $this->respondFailed('Ok', false);
        }

        return $this->respondSuccess('Ok', $model->delete());
    }
}
