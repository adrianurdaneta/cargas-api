<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Order;
use Illuminate\Support\Str;

class CustomerController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->respondSuccess('Ok', Customer::paginate(12));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newCustomer = $request->all();
        $newCustomer['guid'] = Str::uuid()->toString();
        return $this->respondSuccess('Ok', Customer::create($newCustomer));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->respondSuccessGet('Ok', Customer::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Customer::find($id);

        if($model->update($request->all()))
        {
            return $this->respondSuccessGet('Ok', $model);
        }

        return $this->respondFailed('Ok', $model);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Customer::find($id);

        if($model === NULL)
        {
            return $this->respondFailed('Ok', false);
        }

        return $this->respondSuccessGet('Ok', $model->delete());
    }

    public function getByGuid($guid)
    {
        $model = Customer::where('guid', $guid)->first();

        if($model === NULL)
        {
            return $this->respondFailed('Ok', false);
        }

        return $this->respondSuccessGet('Ok', $model);
    }

    public function getOrders($id)
    {
        $model = Customer::find($id)->toArray();

        if($model === NULL)
        {
            return $this->respondFailed('Ok', false);
        }

       $model["orders"] = Order::where("orders.customer_id", $model["id"])
            ->join('customers', 'orders.customer_id', '=', 'customers.id')
            ->join('branches', 'orders.branch_id', '=', 'branches.id')
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->join('vehicles', 'orders.vehicle_id', '=', 'vehicles.id')
            ->join('destinations', 'orders.destination_id', '=', 'destinations.id')
            ->join('drivers', 'orders.driver_id', '=', 'drivers.id')
            ->select('orders.id','orders.order_date','branches.name as branch_name','customers.name as customer_name',
            'users.name as user_name','vehicles.name as vehicle_plate','destinations.name as destination_name','drivers.name as driver_name')
            ->orderBy('orders.order_date','desc')
            ->take(12)->get();

        return $this->respondSuccessGet('Ok', $model);
    }
}
