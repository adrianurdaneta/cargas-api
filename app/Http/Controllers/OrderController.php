<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use Illuminate\Support\Str;

class OrderController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $selectArray = array('orders.id','orders.guid','orders.order_date','branches.name as branch_name','customers.name as customer_name',
            'users.name as user_name','vehicles.name as vehicle_plate','destinations.name as destination_name','drivers.name as driver_name',
            'orders.branch_id', 'orders.vehicle_id', 'orders.customer_id', 'orders.user_id', 'orders.destination_id', 'orders.driver_id', 'orders.cargo_category_id');

        $result = Order::join('customers', 'orders.customer_id', '=', 'customers.id')
            ->join('branches', 'orders.branch_id', '=', 'branches.id')
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->join('vehicles', 'orders.vehicle_id', '=', 'vehicles.id')
            ->join('destinations', 'orders.destination_id', '=', 'destinations.id')
            ->join('drivers', 'orders.driver_id', '=', 'drivers.id')
            ->paginate(12, $selectArray);

        return $this->respondSuccessGet('Ok', $result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $result = $request->all();
        $result["order_date"] = date('Y-m-d H:i:s');
        $result['guid'] = Str::uuid()->toString();
        $id = Order::create($result)->id;

        return $this->show($id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = Order::where("orders.id", $id)
            ->join('customers', 'orders.customer_id', '=', 'customers.id')
            ->join('branches', 'orders.branch_id', '=', 'branches.id')
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->join('vehicles', 'orders.vehicle_id', '=', 'vehicles.id')
            ->join('destinations', 'orders.destination_id', '=', 'destinations.id')
            ->join('drivers', 'orders.driver_id', '=', 'drivers.id')
            ->select('orders.id','orders.guid','orders.order_date','branches.name as branch_name','customers.name as customer_name',
            'users.name as user_name','vehicles.name as vehicle_plate','destinations.name as destination_name','drivers.name as driver_name',
            'orders.branch_id', 'orders.vehicle_id', 'orders.customer_id', 'orders.user_id', 'orders.destination_id', 'orders.driver_id', 'orders.cargo_category_id')
            ->first();

        return $this->respondSuccessGet('Ok', $result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Order::find($id);

        if($model->update($request->all()))
        {
            return $this->show($id);
        }

        return $this->respondFailed('Ok', $model);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Order::find($id);

        if($model === NULL)
        {
            return $this->respondFailed('Ok', false);
        }

        return $this->respondSuccessGet('Ok', $model->delete());
    }
}