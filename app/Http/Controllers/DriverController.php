<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Driver;
use App\QueryHelpers;
use Illuminate\Support\Str;

class DriverController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->respondSuccess('Ok', Driver::paginate(8));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newDriver = $request->all();
        $newDriver['guid'] = Str::uuid()->toString();
        return $this->respondSuccessGet('Ok', Driver::create($newDriver));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->respondSuccessGet('Ok', Driver::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Driver::find($id);

        if($model->update($request->all()))
        {
            return $this->respondSuccessGet('Ok', $model);
        }

        return $this->respondFailed('Ok', $model);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Driver::find($id);

        if($model === NULL)
        {
            return $this->respondFailed('Ok', false);
        }

        return $this->respondSuccessGet('Ok', $model->delete());
    }

    public function destroyMany(Request $request){
        return $this->respondSuccessGet('Ok', Driver::destroy($request->all()));
    }

    public function indexFilter(Request $request)
    {
        return QueryHelpers::filterQuery($request->all()["filters"], Driver::query())->paginate(12);
    }

    public function getByGuid($guid)
    {
        $model = Driver::where('guid', $guid)->first();

        if($model === NULL)
        {
            return $this->respondFailed('Ok', false);
        }

        return $this->respondSuccessGet('Ok', $model);
    }
}