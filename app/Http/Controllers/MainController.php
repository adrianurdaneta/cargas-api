<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

class MainController extends ApiController
{
    public function get_user()
    {
        return $this->respondSuccessGet('Ok', $this->user);
    }

    public function get_all_estados()
    {
        $estados = DB::table('estados')->get();
        return $this->respondSuccessGet('Ok', $estados);
    }

    public function get_all_municipios()
    {
        $municipios = DB::table('municipios')->get();
        return $this->respondSuccessGet('Ok', $municipios);
    }

    public function get_municipios($id)
    {
        $municipios = DB::table('municipios')->where('id_estado', $id)->get();
        return $this->respondSuccessGet('Ok', $municipios);
    }

    public function get_all_parroquias()
    {
        $parroquias = DB::table('parroquias')->get();
        return $this->respondSuccessGet('Ok', $parroquias);
    }

    public function get_parroquias($id)
    {
        $parroquias = DB::table('parroquias')->where('id_municipio', $id)->get();
        return $this->respondSuccessGet('Ok', $parroquias);
    }

    public function get_all_ciudades()
    {
        $ciudades = DB::table('ciudades')->get();
        return $this->respondSuccessGet('Ok', $ciudades);
    }

    public function get_ciudades($id)
    {
        $ciudades = DB::table('ciudades')->where('id_estado', $id)->get();
        return $this->respondSuccessGet('Ok', $ciudades);
    }
}