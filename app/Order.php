<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'guid',
        'branch_id',
        'customer_id',
        'user_id',
        'order_date',
        'comments',
        'vehicle_id',
        'driver_id',
        'destination_id',
        'cargo_category_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }

    public function destination()
    {
        return $this->belongsTo(Destination::class);
    }

    public function cargo_category()
    {
        return $this->belongsTo(CargoCategory::class);
    }
}
