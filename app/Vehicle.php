<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $fillable = [
        'guid', 'name', 'description', 'vehicle_type_id',
    ];

    public function order()
    {
        return $this->hasMany(Order::class);
    }

    public function vehicle_type()
    {
        return $this->belongsTo(VehicleType::class);
    }
}
