<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleType extends Model
{
    protected $fillable = [
        'guid', 'name', 'description',
    ];

    public function vehicle()
    {
        return $this->hasMany(Vehicle::class);
    }
}
