<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    protected $fillable = [
        'guid',
        'name',
        'rif',
        'description',
    ];

    public function order()
    {
        return $this->hasMany(Order::class);
    }
}