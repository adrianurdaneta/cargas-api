<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
                $this->app->bind("League\\Fractal\\ScopeFactoryInterface", "\\League\\Fractal\\ScopeFactory");
    }
    
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
