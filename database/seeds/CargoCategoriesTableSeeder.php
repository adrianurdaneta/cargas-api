<?php

use App\CargoCategory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CargoCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $default_types = [
            [
                'guid' => Str::uuid(),
                'name' => 'Alimentos',
                'description' => 'Productos de para consumo humano',
            ],
            [
                'guid' => Str::uuid(),
                'name' => 'Electrodmésticos',
                'description' => 'Productos Electrónicos para el hogar',
            ],
            [
                'guid' => Str::uuid(),
                'name' => 'Automóviles',
                'description' => 'Automóviles',
            ],
        ];

        foreach ($default_types as $default_type) {
            CargoCategory::create($default_type);
        }
    }
}
