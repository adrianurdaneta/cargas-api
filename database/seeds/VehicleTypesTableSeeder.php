<?php

use App\VehicleType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class VehicleTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $default_types = [
            [
                'guid' => Str::uuid(),
                'name' => 'Camión',
                'description' => 'Camión sencillo',
            ],
            [
                'guid' => Str::uuid(),
                'name' => 'Remolque',
                'description' => 'Camión con Remolque',
            ],
            [
                'guid' => Str::uuid(),
                'name' => 'Remolque Articulado',
                'description' => 'Camión con Remolque Articulado',
            ],
        ];

        foreach ($default_types as $default_type) {
            VehicleType::create($default_type);
        }
    }
}
