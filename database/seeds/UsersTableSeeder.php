<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin_users = [
            [
                'guid' => Str::uuid(),
                'name' => 'Administrador',
                'username' => 'admin',
                'email' => 'admin@admin.com',
                'password' => app('hash')->make('password'),
                'role_id' => 1,
            ],
        ];

        foreach ($admin_users as $admin_user) {
            User::create($admin_user);
        }
    }
}
