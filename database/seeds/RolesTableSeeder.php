<?php

use App\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $default_roles = [
            [
                'guid' => Str::uuid(),
                'name' => 'Administrador',
                'description' => 'Administrador del Sistema',
            ],
            [
                'guid' => Str::uuid(),
                'name' => 'Supervisor',
                'description' => 'Supervisor de Sucursal',
            ],
            [
                'guid' => Str::uuid(),
                'name' => 'Operador',
                'description' => 'Operador de Sucursal',
            ],
            [
                'guid' => Str::uuid(),
                'name' => 'Conductor',
                'description' => 'Conductor de Vehículo',
            ],
            [
                'guid' => Str::uuid(),                
                'name' => 'Cliente',
                'description' => 'Cliente',
            ],
        ];

        foreach ($default_roles as $default_role) {
            Role::create($default_role);
        }
    }
}
