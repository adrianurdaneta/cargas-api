<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDestinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('destinations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('guid')->unique();
            $table->string('name');
            $table->string('description');
            $table->string('address');
            $table->string('rif');
            $table->string('phones');
            $table->bigInteger('id_estado');
            $table->bigInteger('id_municipio');
            $table->bigInteger('id_parroquia');
            $table->bigInteger('id_ciudad');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('destinations');
    }
}
