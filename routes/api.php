<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {

    //public access
    $api->post('/login', [
        'as' => 'api.auth.login',
        'uses' => 'App\Http\Controllers\Auth\AuthController@postLogin',
    ]);

    // secured access
    $api->group(['middleware' => 'api.auth'], function ($api) {

        $api->get('/CurrentUser', 'App\Http\Controllers\MainController@get_user');
        $api->get('/Estados', 'App\Http\Controllers\MainController@get_all_estados');
        $api->get('/Municipios', 'App\Http\Controllers\MainController@get_all_municipios');
        $api->get('/Municipios/{id}', 'App\Http\Controllers\MainController@get_municipios');
        $api->get('/Parroquias', 'App\Http\Controllers\MainController@get_all_parroquias');
        $api->get('/Parroquias/{id}', 'App\Http\Controllers\MainController@get_parroquias');
        $api->get('/Ciudades', 'App\Http\Controllers\MainController@get_all_ciudades');
        $api->get('/Ciudades/{id}', 'App\Http\Controllers\MainController@get_ciudades');

        $api->resource('/User', 'App\Http\Controllers\UserController');
        $api->resource('/CargoCategory', 'App\Http\Controllers\CargoCategoryController');
        $api->get('/CargoCategoryByGuid/{guid}', 'App\Http\Controllers\CargoCategoryController@getByGuid');
        $api->get('/CargoCategoryList', 'App\Http\Controllers\CargoCategoryController@fullList');
        $api->resource('/VehicleType', 'App\Http\Controllers\VehicleTypeController');
        $api->resource('/Role', 'App\Http\Controllers\RoleController');
        $api->resource('/Driver', 'App\Http\Controllers\DriverController');
        $api->post('/Driver/Del', 'App\Http\Controllers\DriverController@destroyMany');
        $api->post('/Driver/Filter', 'App\Http\Controllers\DriverController@indexFilter');
        $api->get('/DriverByGuid/{guid}', 'App\Http\Controllers\DriverController@getByGuid');
        $api->resource('/Branch', 'App\Http\Controllers\BranchController');
        $api->resource('/Order', 'App\Http\Controllers\OrderController');
        $api->resource('/Vehicle', 'App\Http\Controllers\VehicleController');
        $api->get('/VehicleByGuid/{guid}', 'App\Http\Controllers\VehicleController@getByGuid');
        $api->resource('/Customer', 'App\Http\Controllers\CustomerController');
        $api->get('/CustomerByGuid/{guid}', 'App\Http\Controllers\CustomerController@getByGuid');
        $api->get('/CustomerDetails/{id}', 'App\Http\Controllers\CustomerController@getOrders');
        $api->resource('/Destination', 'App\Http\Controllers\DestinationController');
        $api->get('/DestinationByGuid/{guid}', 'App\Http\Controllers\DestinationController@getByGuid');

    });

});